import pyglet
import random
import math
from vector import make_destination
from labirynth import make_field
from pyglet.window import key
from moves import make_the_right_way
from moves import sort_enemies



class Game:
    def __init__(self):
        self.window = pyglet.window.Window(width=1056, height=480)
        self.wall = pyglet.image.load('brick_brown-vines3.png')
        self.floor = pyglet.image.load('floor_vines2.png')
        self.player_image = pyglet.image.load('player.png')
        self.enemy_image = pyglet.image.load('enemy.png')
        self.field = make_field(5,33,15)
        self.window.on_key_press = self.controller        
        self.sprites = []
        self.empty_places = []
        self.walls = []
        self.T = 0
        self.vy = 0
        self.G = 9.81
        for i,line in enumerate(self.field):
            for j,character in enumerate(line):
                if character ==' ':
                    self.walls.append((j,i))
                    sprite = pyglet.sprite.Sprite(img=self.wall, x=32*j , y=32*i)
                else:
                    self.empty_places.append((j,i))
                    sprite = pyglet.sprite.Sprite(img=self.floor, x=32*j , y=32*i)                    
                self.sprites.append(sprite)
        x1,y1 = random.choice(self.empty_places)
        self.player = pyglet.sprite.Sprite(img=self.player_image, x=32*x1, y =32*y1)
        self.player.health = 10
        self.enemies=[]
        for i in range(10):           
            x2,y2 = random.choice(self.empty_places)
            enemy = pyglet.sprite.Sprite(img=self.enemy_image, x=32*x2, y=32*y2)
            self.enemies.append(enemy)
        self.label = pyglet.text.Label( f'Score: {self.player.health}',
                                  font_name='Times New Roman',
                                  font_size=36,
                                  x=self.window.width//2, y=self.window.height//2,
                                  anchor_x='center', anchor_y='center')
        
        self.window.on_draw = self.on_draw
        pyglet.app.run()
           
    
    def apply_gravity(self, G, dt):
        if self.fire.y > 0:
            self.vy -= G*dt
            self.fire.y += self.vy + self.vy*dt
            pyglet.clock.unschedule(self.player.update)
        else:
            self.vy = 0
            self.fire.y = 0

    def update(self, dt):
            self.T += dt
            self.apply_gravity(self.G, dt)
            

    def moovement(self, dt):
        self.vy = 32*dt
        self.fire.y += self.vy + self.vy*dt
        pyglet.clock.schedule(self.update)
    def controller(self, symbol, modifiers):
        if symbol == key.UP:
            xs = []
            for f in range(len(self.walls)):
                a1,b1 = self.walls[f]
                if self.player.x == 32*a1 and self.player.y+32 == 32*b1:
                    xs.append((a1,b1))
                else:
                    pass
            if len(xs) == 0:
                self.player.y += 32
            
        elif symbol == key.DOWN:
            xs = []
            for f in range(len(self.walls)):
                a1,b1 = self.walls[f]
                if self.player.x == 32*a1 and self.player.y-32 == 32*b1:
                    xs.append((a1,b1))
            if len(xs) == 0:
                self.player.y -= 32
            else:
                pass
            
        elif symbol == key.LEFT:
            xs = []
            for f in range(len(self.walls)):
                a1,b1 = self.walls[f]
                if self.player.x-32 == 32*a1 and self.player.y == 32*b1:
                    xs.append((a1,b1))
                else:
                    pass
            if len(xs) == 0:
                self.player.x -= 32
        elif symbol == key.RIGHT:
            xs = []
            for f in range(len(self.walls)):
                a1,b1 = self.walls[f]
                if self.player.x+32 == 32*a1 and self.player.y == 32*b1:
                    xs.append((a1,b1))
                else:
                    pass
            if len(xs) == 0:
                self.player.x += 32


                   
        x2 = self.player.x//32
        y2= self.player.y//32
        enemy=self.enemies[0]
        print(self.field)
        new_enemies=sort_enemies(self.enemies,x2,y2,self.field)
        print(new_enemies)
        for enemy in new_enemies:
            position=enemy
            for walls in self.walls:
                if x1==walls[0] and y1==walls[1]:
                    empty_places+=1
            if empty_places==0:
                if self.player.x==enemy.x and self.player.y==enemy.y:
                    self.player.health-=1
                    enemy.x = enemy.x+dx*32
                    enemy.y = enemy.y+dy*32
                    self.label.text = f'Score: {self.player.health}' #что-то я не очень понял, зачем мы два раза прибавдяем движение противника, а не вычитаем его
            else:
                x1,y1=make_the_right_way(self.field, x2,y2,x1,y1)
                
                enemy.x=x1*32
                enemy.y=y1*32

            
    def on_draw(self):
        self.window.clear()
        for sprite in self.sprites:
            sprite.draw()
        self.player.draw()
        for enemy in self.enemies:
            enemy.draw()
        self.label.draw()
        
        
    



        
        
Game()




