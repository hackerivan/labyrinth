import random

# Требования:
# - при создании любого прямоугольника, проверять что он корректный
# первая точка - левый верхний угол, вторая точка - правый нижний
# - как можно больше кода должно продолжать работать

class Rectangle(list):
    def __init__(self, p1, p2):
        super().__init__([p1, p2])
        self.br = p2
        self.tl = p1
        x1, y1 = p1
        x2, y2 = p2
        self.t = y1
        self.b = y2
        self.r = x2
        self.l = x1
        self.bl = (self.l, self.b)
        #self.tr
        if not (x1<x2 and y1<y2):
            raise ValueError('p1 should be top-left point, p2 should be bottom-right point')
    def __eq__(self, other):
        if isinstance(other, Rectangle):
            return self.br==other.br and self.tl==other.tl
        else:
            return tuple(self)==other

def make_point(w,h):
    x1=random.randint(0,w-1)
    y1=random.randint(0,h-1)
    return x1, y1



def make_rectangle2(w,h):
    x1,y1=make_point(w-1,h-1)
    x2,y2=make_point(w-1,h-1)
    while True:
        if x1>x2 and y1>y2:
            return Rectangle((x2,y2),(x1,y1))
        elif x1==x2 and y1>y2:
            pass
        elif x1<x2 and y1>y2:
            return Rectangle((x1,y2),(x2,y1))
        elif x1>x2 and y1==y2:
            pass
        elif x1<x2 and y1==y2:
            pass
        elif x1==x2 and y1==y2:
            pass
        elif x1<x2 and y1<y2:
            return Rectangle((x1,y1),(x2,y2))    
        elif x1==x2 and y1<y2:
            pass
        elif x1>x2 and y1<y2:
            return Rectangle((x2,y1),(x1,y2))
        else:
            print ('нерассмотренный случай')
        x2,y2=make_point(w-1,h-1)





def is_in_segment(point,segment):
    x1,y1=point
    ((x2,y2),(x3,y3))=segment
    if int(min(x2,x3))<=x1<=int(max(x2,x3)) and int(min(y2,y3))<=y1<=int(max(y2,y3)):
        return True
   

def make_field(n, w, h):
    rects=make_not_crossing_rectangles(n, w, h)
    edges=make_edges(rects)
    paths = [make_path(edge) for edge in edges]
    paths = [segment for path in paths for segment in path]


    points = [[' ' for j in range(w)] for i in range(h)]

    for i in range(h):
     for j in range(w):

         for path in paths:
             point=(j,i)
             if is_in_segment(point,path):
                 points[i][j]='-'
                 
                 break
     
             
    for i in range(h):
     for j in range(w):
         
         for  a,rectangle in enumerate(rects, 1):
             ((x1,y1),(x2,y2)) = rectangle
             if x1<=j<=x2 and y1<=i<=y2:
                 points[i][j]=a
                 
                 break
    return points




            
def is_in_rectangle(point ,rect):
    x,y=point
    ((x1,y1),(x2,y2))=rect
    return x1<=x<=x2 and y1<=y<=y2

def make_not_crossing_rectangles(n,w,h):
    xs=[]
    rect1= make_rectangle2(w,h)
    xs.append(rect1)
    while len(xs)<n:
        ((x3,y3),(x4,y4))= make_rectangle2(w,h)
        rect=((x3,y3),(x4,y4))
        tl=(x3,y3)
        tr=(x4,y3)
        bl=(x3,y4)
        br=(x4,y4)
        for i in range(len(xs)):
            ((x1,y1),(x2,y2))=xs[i]
            tli=(x1,y1)
            tri=(x2,y1)
            bli=(x1,y2)
            bri=(x2,y2)
            if is_in_rectangle(tl ,xs[i]) or is_in_rectangle(tr ,xs[i])or is_in_rectangle(bl,xs[i]) or is_in_rectangle(br ,xs[i]):
                break
            if is_in_rectangle(tli ,rect) or is_in_rectangle(tri ,rect)or is_in_rectangle(bli,rect) or is_in_rectangle(bri ,rect):
                break
            if x1<x3<x4<x2 and y3<y1<y2<y4:
                break
            if x3<x1<x2<x4 and y1<y3<y4<y2:
                break
        else:
            xs.append(rect)
    return xs



def find_center(rect):
        ((x1,y1),(x2,y2))=rect
        x3=(x1+x2)/2
        y3=(y1+y2)/2
        return (x3,y3)

        

def make_edges(rects):
    centers=[find_center(rect) for rect in rects]
    center1, center2 = random.sample(centers,2)
    edges = [(center1, center2)]
    centers.remove(center1)
    centers.remove(center2)
    graph=[]
    graph.append(center1)
    graph.append(center2)
    while len(centers)>0:
        v1=random.choice(centers)
        v2=random.choice(graph)
        edges.append((v1,v2))
        graph.append(v1)
        centers.remove(v1)
    return edges

def make_path(segment):
    ((x1,y1),(x2,y2))=segment
    if x1==x2 and y2<y1:
        return [((x1,y1),(x2,y2))] #2
    elif x1==x2 and y1<y2:
        return [((x1,y1),(x2,y2))] #6
    elif y1==y2 and x1>x2:
        return [((x1,y1),(x2,y2))] #8
    elif y1==y2 and x1<x2:
        return [((x1,y1),(x2,y2))] #4
    elif x2<x1 and y2>y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #7
    elif x2>x1 and y2>y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #5
    elif x2>x1 and y2<y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #3
    elif x2<x1 and y2<y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #1
    elif x1==x2 and y1==y2:
        return []
    else:
        raise ValueError('Нерассмотренный случай')

def print_field(field):
    for row in field:
        for cell in row:
            print(cell, sep='', end='')
        print()
    





def find_center(rect):
        ((x1,y1),(x2,y2))=rect
        x3=(x1+x2)/2
        y3=(y1+y2)/2
        return (x3,y3)
        
def make_path(segment):
    ((x1,y1),(x2,y2))=segment
    if x1==x2 and y2<y1:
        return [((x1,y1),(x2,y2))] #2
    elif x1==x2 and y1<y2:
        return [((x1,y1),(x2,y2))] #6
    elif y1==y2 and x1>x2:
        return [((x1,y1),(x2,y2))] #8
    elif y1==y2 and x1<x2:
        return [((x1,y1),(x2,y2))] #4
    elif x2<x1 and y2>y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #7
    elif x2>x1 and y2>y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #5
    elif x2>x1 and y2<y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #3
    elif x2<x1 and y2<y1:
        return [((x1,y1),(x2,y1)),((x2,y1),(x2,y2))] #1
    elif x1==x2 and y1==y2:
        return []
    else:
        raise ValueError('Нерассмотренный случай')

  
    
    













