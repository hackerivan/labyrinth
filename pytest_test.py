from labirynth import *

def test_make_rectangle2():
    for _ in range(1000):
        p1, p2 = make_rectangle2(5,5)
        x1, y1 = p1
        x2, y2 = p2
        assert x1<x2 and y1<y2

def test_make_point():
    for _ in range(1000):
        x, y = make_point(5, 5)
        assert type(x) == int and type(y) == int and 0<=x<5 and 0<=y<5
        
