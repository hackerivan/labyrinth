#from pprint import pprint as print
import random
import math
from labirynth import make_field


               

def print_field(field):
    for row in field:
        for cell in row:
            if cell is None:
                print(None, end=' ')
            else:
                print(f'{cell:4}', end=' ')
        print()
    print()


def make_the_right_way(field,x2,y2):
    def recur(field,y2,x2,s):
        """
        nonlocal c
        c += 1
        if c//1000000>0:
            print(c)
            c = 0
        """
        print_field(field)
        field[y2][x2]=s
        
        if y2<len(field)-1 and (field[y2+1][x2]==None or field[y2][x2]-1<field[y2+1][x2]) \
           and field[y2+1][x2]!=500:
            recur(field,y2+1,x2,s+1)
            
        if x2<len(field[0])-1 and (field[y2][x2+1]==None or field[y2][x2]-1<field[y2][x2+1])\
           and field[y2][x2+1]!=500:
            recur(field,y2,x2+1,s+1)
            
        if y2>0 and (field[y2-1][x2]==None or field[y2][x2]-1<field[y2-1][x2])\
           and field[y2-1][x2]!=500:
            recur(field,y2-1,x2,s+1)
            
        if x2>0 and (field[y2][x2-1]==None or field[y2][x2]-1<field[y2][x2-1])\
           and field[y2][x2-1]!=500:
            recur(field,y2,x2-1,s+1)
        return field
    c = 0    
    floor_or_walls=[]
    for i,line in enumerate(field):  #строим поле и заменяем стены на значение равное 500
        row = []
        for j,character in enumerate(line):
            if character ==' ':
                d=500
                row.append(d)
            else:
                row.append(None)
        floor_or_walls.append(row)
    a=y2
    b=x2
    f=0
    floor_or_walls= recur(floor_or_walls,a,b,f)

    
    return floor_or_walls
            



def sort_enemies(enemies,x1,y1,field):
    breakpoint()
    print(1)
    floor_or_walls=make_the_right_way(field,x1,y1)
    print(2)
    new_enemies=[]
    for enemy in enemies:
        x=enemy.x//32
        y=enemy.y//32
        number=floor_or_walls[y][x]
        new_enemies.append((number, enemy))
    print(3)
    return sorted(new_enemies, key=lambda x: x[0])
        

        
    
"""
floor_or_walls=make_the_right_way(field,x2,y2,x1,y1)
print(sort_enemies(enemies,x2,y2,floor_or_walls))
"""
