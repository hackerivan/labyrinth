import random
import math


def make_destination(x1,y1,x2,y2):
    xs = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(1,0)]
    tan = math.atan2((y2-y1),(x2-x1))
    tan1 = math.degrees(tan)
    if tan1>=0:
        tan1 = tan1
    else:
        tan1 = tan1+360
    
    tangens = [ (abs(tan1-45*i), xs[i]) for i in range(9)]
    a = min(tangens)

    dx=a[1][0]
    dy=a[1][1]

    return  dx,dy

dx,dy=make_destination(20,33,26,27)
print(dx,dy)
print('======')

assert make_destination(1, 20, 35, 18) == (1, 0)
assert make_destination(20, 33, 26, 27) == (1, -1)
assert make_destination(39, 19, 25, 13) == (-1, -1)




























'''
assert make_destination(1, 20, 35, 18) == (2, 20)
assert make_destination(20, 33, 26, 27) == (21, 32)
assert make_destination(39, 19, 25, 13) == (38, 18)
'''


'''
def make_destination(x1,y1,x2,y2):
    z = math.atan2((y2-y1),(x2-x1))
    z = math.degrees(z)
    y=y1
    x=x1

    if z < 0:
        z=z+360
    else:
        z=z
    

    tan = [ math.degrees((math.pi)*x/4) for x in range(9)]

    m=360
    c=0
    for i in tan:
        n = abs( i-z)
        if n<m:
            m=n
            c=i
     # мы нашли угол, максимально близкий к вектору, а m-их разница


    if c==0.0 or c==360.0:
        x=x+1
    elif c==45.0:
        x=x+1
        y=y+1
    elif c==90.0:
        y=y+1
    elif c==135.0:
        x=x-1
        y=y+1
    elif c==180.0:
        x=x-1
    elif c==225.0:
        x=x-1
        y=y-1
    elif c==270.0:
        y=y-1
    elif c==315.0:
        x=x+1
        y=y-1

    return x,y
'''
  


