import pytest

def make_the_right_way(field,x2,y2):
    def recur(field,y2,x2,s):
        nonlocal path
        path.append((x2, y2))
        field[y2][x2]=s
        if y2<len(field)-1 and (field[y2+1][x2]==None or field[y2][x2]-1<field[y2+1][x2]) \
           and field[y2+1][x2]!=500:
            breakpoint()
            recur(field,y2+1,x2,s+1)
        if x2<len(field[0])-1 and (field[y2][x2+1]==None or field[y2][x2]-1<field[y2][x2+1])\
           and field[y2][x2+1]!=500:
            breakpoint()
            recur(field,y2,x2+1,s+1)
        if y2>0 and (field[y2-1][x2]==None or field[y2][x2]-1<field[y2-1][x2])\
           and field[y2-1][x2]!=500:
            breakpoint()
            recur(field,y2-1,x2,s+1)
        if x2>0 and (field[y2][x2-1]==None or field[y2][x2]-1<field[y2][x2-1])\
           and field[y2][x2-1]!=500:
            breakpoint()
            recur(field,y2,x2-1,s+1)
        return field

    path = []
    floor_or_walls= recur(field,y2,x2,0)
    return floor_or_walls, path


def breadth_first_walk(field, x2, y2):
    current_level = {(x2, y2)}
    next_level = set()
    level = 0
    path = []
    while current_level:
        for x, y in current_level:
            path.append((x, y))
            field[y][x] = level
            if y<len(field)-1 and (field[y+1][x]==None or field[y][x]-1<field[y+1][x]) \
               and field[y+1][x]!=500:
                print(x,y)
                next_level.add((x, y+1))
            if x<len(field[0])-1 and (field[y][x+1]==None or field[y][x]-1<field[y][x+1])\
               and field[y][x+1]!=500:
                print(x,y)
                next_level.add((x+1, y))
            if y>0 and (field[y-1][x]==None or field[y][x]-1<field[y-1][x])\
               and field[y-1][x]!=500:
                print(x,y)
                next_level.add((x, y-1))
            if x>0 and (field[y][x-1]==None or field[y][x]-1<field[y][x-1])\
               and field[y][x-1]!=500:
                next_level.add((x-1, y))
        level += 1
        current_level = next_level
        next_level = set()
    return field, path


cells = {
        (0, 0): "A",
        (1, 0): "B",
        (2, 0): "C",
        (0, 1): "D",
        (1, 1): "E",
        (2, 1): "F",
        (0, 2): "G",
        (1, 2): "H",
        (2, 2): "I",
        (0, 3): "J",
        (1, 3): "K",
        (2, 3): "L",
    }


'''
@pytest.mark.xfail
'''


def test_depth_first():
    field = [[None for _ in range(3)] for _ in range(4)]
    breakpoint()
    distance_map, path = make_the_right_way(field, 1,1)
    result = ''.join([cells[point] for point in path])
    assert result == "EHKLIFCBADGJJGDAIFCBGDAFCBBAD"
    

def test_breadth_first():
    field = [[None for _ in range(3)] for _ in range(4)]
    distance_map, path = breadth_first_walk(field, 1,1)
    result = ''.join([cells[point] for point in path])
    assert result == "EHFBDKIGCALJ"
